class TranslinksController < ApplicationController
  before_action :set_translink, only: [:show, :edit, :update, :destroy]

  # GET /translinks
  # GET /translinks.json
  def index
    @translinks = Translink.all
  end

  # GET /translinks/1
  # GET /translinks/1.json
  def show
  end

  # GET /translinks/new
  def new
    @translink = Translink.new
  end

  # GET /translinks/1/edit
  def edit
  end

  # POST /translinks
  # POST /translinks.json
  def create
    @translink = Translink.new(translink_params)

    respond_to do |format|
      if @translink.save
        format.html { redirect_to @translink, notice: 'Translink was successfully created.' }
        format.json { render :show, status: :created, location: @translink }
      else
        format.html { render :new }
        format.json { render json: @translink.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /translinks/1
  # PATCH/PUT /translinks/1.json
  def update
    respond_to do |format|
      if @translink.update(translink_params)
        format.html { redirect_to @translink, notice: 'Translink was successfully updated.' }
        format.json { render :show, status: :ok, location: @translink }
      else
        format.html { render :edit }
        format.json { render json: @translink.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /translinks/1
  # DELETE /translinks/1.json
  def destroy
    @translink.destroy
    respond_to do |format|
      format.html { redirect_to translinks_url, notice: 'Translink was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def delete_matching

    if File.exist?("Matching.txt")
      File.delete("Matching.txt")
    end
    if File.exist?("Zielfunktionswert.txt")
      File.delete("Zielfunktionswert.txt")
    end

    @translinks = Translink.all
    @translinks.each { |li|
  #      li.matching=0.0
      li.matching=nil
      li.save
    }

    @objective_function_value=nil

    render :template => "translinks/index"

  end

  def read_transportation_quantities

    if File.exist?("Matching.txt")

      fi=File.open("Matching.txt", "r")
      fi.each { |line| # printf(f,line)
        sa=line.split(";")
        sa0=sa[0].delete "l "
        sa3=sa[3].delete " \n"
        al=Translink.find_by_id(sa0)
        al.matching=sa3
        al.save

      }
      fi.close
      @translinks = Translink.all
      render :template => "translinks/index"
    else
      flash.now[:not_available] = "Matching isn't yet computed!"
      @translinks = Translink.all
      render :template => "translinks/index"
    end
  end


  def read_and_show_ofv

    if File.exist?("Zielfunktionswert.txt")
      fi=File.open("Zielfunktionswert.txt", "r")
      line=fi.readline
      fi.close
      sa=line.split(" ")
      @objective_function_value=sa[1]
    else
      @objective_function_value=nil
      flash.now[:not_available] = "Objective Function Value isn't yet computed!"

    end

    @translinks = Translink.all
    render :template => "translinks/index"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_translink
      @translink = Translink.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def translink_params
      params.require(:translink).permit(:student_id, :institute_id, :preference_value, :matching)
    end
end
