class Institute < ApplicationRecord
    has_many :students, dependent: :destroy
    has_many :translinks, :dependent => :destroy
    accepts_nested_attributes_for :translinks
end
