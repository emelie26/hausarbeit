Rails.application.routes.draw do
  resources :translinks
  resources :institutes
  resources :students
  root   'static_pages#home'
  get    '/help', to: 'static_pages#help'
  get    '/about', to: 'static_pages#about'
  get    '/contact', to: 'contacts#new'
  get    '/signup', to: 'users#new'
  get    '/login', to: 'sessions#new'
  get    'matching_start', to: 'static_pages#matching_start'
  post   '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  match '/contacts', to: 'contacts#new', via: 'get'
  resources :contacts, only: [:new, :create]
  resources :users
  post 'translinks/delete_matching', :to => 'translinks#delete_matching'
  post 'translinks/read_matching', :to => 'translinks#read_matching'
  post 'translinks/read_and_show_ofv', :to => 'translinks#read_and_show_ofv'
end
