require 'test_helper'

class TranslinksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @translink = translinks(:one)
  end

  test "should get index" do
    get translinks_url
    assert_response :success
  end

  test "should get new" do
    get new_translink_url
    assert_response :success
  end

  test "should create translink" do
    assert_difference('Translink.count') do
      post translinks_url, params: { translink: { institute_id: @translink.institute_id, matching: @translink.matching, preference_value: @translink.preference_value, student_id: @translink.student_id } }
    end

    assert_redirected_to translink_url(Translink.last)
  end

  test "should show translink" do
    get translink_url(@translink)
    assert_response :success
  end

  test "should get edit" do
    get edit_translink_url(@translink)
    assert_response :success
  end

  test "should update translink" do
    patch translink_url(@translink), params: { translink: { institute_id: @translink.institute_id, matching: @translink.matching, preference_value: @translink.preference_value, student_id: @translink.student_id } }
    assert_redirected_to translink_url(@translink)
  end

  test "should destroy translink" do
    assert_difference('Translink.count', -1) do
      delete translink_url(@translink)
    end

    assert_redirected_to translinks_url
  end
end
