class AddInstituteIdToStudents < ActiveRecord::Migration[5.1]
  def change
    add_column :students, :institute_id, :string
  end
end
