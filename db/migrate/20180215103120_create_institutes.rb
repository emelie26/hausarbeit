class CreateInstitutes < ActiveRecord::Migration[5.1]
  def change
    create_table :institutes do |t|
      t.string :name
      t.integer :number_of_professors
      t.integer :number_of_employees
      t.float :excess_capacity

      t.timestamps
    end
  end
end
