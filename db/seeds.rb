# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


User.create!(name:  "Example User",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true)

User.create!(name:  "Stefan Helber",
            email: "stefan.helber@prod.uni-hannover.de",
            password:              "geheim",
            password_confirmation: "geheim",
            admin: true)

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)
end

Inst1 = Institute.create!(number_of_professors: 2, number_of_employees: 5, excess_capacity: 0.09)
Inst2 = Institute.create!(number_of_professors: 1, number_of_employees: 7, excess_capacity: 0.1)
Inst3 = Institute.create!(number_of_professors: 1, number_of_employees: 8, excess_capacity: 0.15)
Inst4 = Institute.create!(number_of_professors: 3, number_of_employees: 6, excess_capacity: 0.13)

Stud1 = Student.create!(studentnumber: 100, institute: Inst1, preference_value: 4)
Stud2 = Student.create!(studentnumber: 200, institute: Inst2, preference_value: 5)
Stud3 = Student.create!(studentnumber: 300, institute: Inst3, preference_value: 7)

TraLi1 = Translink.create!(student_id: Stud1.id, institute_id: Inst1.id, preference_value: 6, matching: 0.0)
TraLi2 = Translink.create!(student_id: Stud1.id, institute_id: Inst2.id, preference_value: 2, matching: 0.0)
TraLi3 = Translink.create!(student_id: Stud1.id, institute_id: Inst3.id, preference_value: 6, matching: 0.0)
TraLi4 = Translink.create!(student_id: Stud1.id, institute_id: Inst4.id, preference_value: 7, matching: 0.0)
TraLi5 = Translink.create!(student_id: Stud2.id, institute_id: Inst1.id, preference_value: 4, matching: 0.0)
TraLi6 = Translink.create!(student_id: Stud2.id, institute_id: Inst2.id, preference_value: 9, matching: 0.0)
TraLi7 = Translink.create!(student_id: Stud2.id, institute_id: Inst3.id, preference_value: 5, matching: 0.0)
TraLi8 = Translink.create!(student_id: Stud2.id, institute_id: Inst4.id, preference_value: 3, matching: 0.0)
TraLi9 = Translink.create!(student_id: Stud3.id, institute_id: Inst1.id, preference_value: 8, matching: 0.0)
TraLi10 = Translink.create!(student_id: Stud3.id, institute_id: Inst2.id, preference_value: 8, matching: 0.0)
TraLi11 = Translink.create!(student_id: Stud3.id, institute_id: Inst3.id, preference_value: 1, matching: 0.0)
TraLi12 = Translink.create!(student_id: Stud3.id, institute_id: Inst4.id, preference_value: 6, matching: 0.0)
