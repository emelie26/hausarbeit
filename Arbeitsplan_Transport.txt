Arbeitsplan für eine rudimentäre Transportmodell-App:


Umbau des Headers zum Verweis auf die OM-Apps, eingefügt vor "Account":

  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
      OM-Apps <b class="caret"></b>
    </a>
    <ul class="dropdown-menu">
      <li><%= link_to "Transport", transport_start_path %></li>
      <li><%= link_to "MLCLSP", '#' %></li>
    </ul>
  </li>

Nun ist natürlich der Pfad für 'transport_start_path' noch nicht definiert, das erfolgt im File routes.rb:

  get 'transport_start', to: 'static_pages#transport_start'

Jetzt benötigen wir eine statische Seite für den Start der Transportplanung:

Datei: transport_start.html.erb


  <% provide(:title, 'Transportplanung') %>
  <% if logged_in? %>

  <h1>Klassisches Transportmodell</h1>
  <p>
  Auf dieser Seite startet die Transportplanung!    <br>     <br>

  <%= link_to "Hier geht es zur Verwaltung der Orte!", sites_path %><br>
  <%= link_to "Hier geht es zur Verwaltung der Angebotsdaten!", '#supplysites_path' %><br>
  <%= link_to "Hier geht es zur Verwaltung der Nachfragedaten!", '#demandsites_path' %><br>
  <%= link_to "Hier geht es zur Verwaltung der Transportrelationen!", '#translinks_path' %><br>
  </p>

  <br>


  <% end %>

Hier ist bereits eine benannte Route auf die Ressource 'Site' angelegt, verwenden wir der Einfachheit halber jetzt einen Scaffold für die Orte. Zunächst legen wir die Orte an, dort benötigen wir einen Klarnamen und IATA-Codes:

$ rails generate scaffold Site name:string codename:string


Nun wäre es schön, wenn wir ein paar geeignete Orte hätten, ohne die immer wieder neu anlegen zu müssen:

Dazu ergänzen wir die Datei "seeds.rb":

  S1 = Site.create!(name: "Frankfurt(M)",
                    codename: "FRA")
  S2 = Site.create!(name: "Berlin-Tegel",
                    codename: "TLX")
  S3 = Site.create!(name: "München",
                    codename: "MUC")
  S4 = Site.create!(name: "Bremen",
                    codename: "BRE")
  S5 = Site.create!(name: "Cottbus",
                    codename: "CBU")
  S6 = Site.create!(name: "Düsseldorf",
                    codename: "DUS")
  S7 = Site.create!(name: "Hamburg",
                    codename: "HAM")
  S8 = Site.create!(name: "Stuttgart",
                    codename: "STR")


  (10..99).each do |n|
    name = "Beispielort-#{n}"
    codename = "C#{n}"
    Site.create!(name: name,
                 codename: codename)
  end



$ rails db:migrate:reset
$ rails db:seed


Nun wollen wir aus diesen Orten jene auswählen, an denen ein Angebot oder eine Nachfrage vorliegt. Fangen wir mit den Angebotsorten an. Für jeden Angebotsort müssen wir die Angebotsmenge und die ID der zugrundeliegenden Site speichern können:

$ rails generate scaffold Supplysite supply_quantity:float site:references

Analog machen wir das mit den Nachfrageorten:

$ rails generate scaffold Demandsite demand_quantity:float site:references

Nun wollen wir auch dafür Musterdaten haben in der Datei "seeds.rb":

  SuSi1 = Supplysite.create!(site_id: S1.id, supply_quantity: 20)

  SuSi2 = Supplysite.create!(site_id: S2.id, supply_quantity: 25)

  SuSi3 = Supplysite.create!(site_id: S3.id, supply_quantity: 21)

  DeSi1 = Demandsite.create!(site_id: S4.id, demand_quantity: 15)

  DeSi2 = Demandsite.create!(site_id: S5.id, demand_quantity: 17)

  DeSi3 = Demandsite.create!(site_id: S6.id, demand_quantity: 22)

  DeSi4 = Demandsite.create!(site_id: S7.id, demand_quantity: 12)


Wir führen die Migration durch und befüllen die Datenbank:

$ rails db:migrate:reset
$ rails db:seed


Ein Aufruf von "http://localhost:3000/supplysites" zeigt uns, dass jetzt die zugrundeliegenden Orte nicht richtig angezeigt werden, sondern der Verweis. Der hilft uns aber nicht nicht.


Im Index-File der Supplysites passen wir an:

<thead>
  <tr>
    <th>Supply quantity</th>
    <th>Code</th>
    <th>Name</th>
    <th colspan="3"></th>
  </tr>
</thead>

<tbody>
  <% @supplysites.each do |supplysite| %>
    <tr>
      <td><%= supplysite.supply_quantity %></td>
      <td><%= supplysite.site.codename %></td>
      <td><%= supplysite.site.name %></td>
      <td><%= link_to 'Show', supplysite %></td>
      <td><%= link_to 'Edit', edit_supplysite_path(supplysite) %></td>
      <td><%= link_to 'Destroy', supplysite, method: :delete, data: { confirm: 'Are you sure?' } %></td>
    </tr>
  <% end %>
</tbody>

Analog bei den Demandsites:

<thead>
  <tr>
    <th>Demand quantity</th>
    <th>Code</th>
    <th>Name</th>
    <th colspan="3"></th>
  </tr>
</thead>

<tbody>
  <% @demandsites.each do |demandsite| %>
    <tr>
      <td><%= demandsite.demand_quantity %></td>
      <td><%= demandsite.site.codename %></td>
      <td><%= demandsite.site.name %></td>
      <td><%= link_to 'Show', demandsite %></td>
      <td><%= link_to 'Edit', edit_demandsite_path(demandsite) %></td>
      <td><%= link_to 'Destroy', demandsite, method: :delete, data: { confirm: 'Are you sure?' } %></td>
    </tr>
  <% end %>
</tbody>

Nun möchten wir von der Startseite der Transportplanung dahin navigieren können. Glücklicherweise gibt es die benamten Routen, so dass wir die Links jetzt aktivieren können in der View-Datei "transport_start.html.erb":

<%= link_to "Hier geht es zur Verwaltung der Angebotsdaten!", supplysites_path %><br>
<%= link_to "Hier geht es zur Verwaltung der Nachfragedaten!", demandsites_path %><br>


Allerdings müssen wir nun noch beim Anlegen, Zeigen und Editieren der Angebots- und Nachfrageorte auf die zugrundliegenden Sites referenzieren. Beispielhaft sieht das für die Angebotsorte folgendermaßen aus:

Datei show.html.erb:


  <p>
    <strong>Site:</strong>
    <%= @supplysite.site.name %>
  </p>



Datei _form.html.erb:


  <div class="field">
    <%= form.label 'Code auswählen' %>
    <%= form.collection_select :site_id, Site.all, :id, :codename %>
  </div>

  <div class="field">
    <%= form.label 'Angebotsmenge eingeben' %>
    <%= form.text_field :supply_quantity, id: :supplysite_supply_quantity %>
  </div>


In den Dateien index.html.erb fügen wir unten noch einen Link für den Rücksprung ein:

<br>
<%= link_to 'Zurück zur Transportplanung', transport_start_path %>


Nun brauchen wir die Transportrelationen:

$ rails generate scaffold Translink supplysite_id:integer demandsite_id:integer unit_cost:float transport_quantity:float

Auch hier befüllen wir die Datei "seeds.rb":

TraLi1 = Translink.create!(supplysite_id: SuSi1.id, demandsite_id: DeSi1.id, unit_cost: 6, transport_quantity: 0.0)
TraLi2 = Translink.create!(supplysite_id: SuSi1.id, demandsite_id: DeSi2.id, unit_cost: 2, transport_quantity: 0.0)
TraLi3 = Translink.create!(supplysite_id: SuSi1.id, demandsite_id: DeSi3.id, unit_cost: 6, transport_quantity: 0.0)
TraLi4 = Translink.create!(supplysite_id: SuSi1.id, demandsite_id: DeSi4.id, unit_cost: 7, transport_quantity: 0.0)
TraLi5 = Translink.create!(supplysite_id: SuSi2.id, demandsite_id: DeSi1.id, unit_cost: 4, transport_quantity: 0.0)
TraLi6 = Translink.create!(supplysite_id: SuSi2.id, demandsite_id: DeSi2.id, unit_cost: 9, transport_quantity: 0.0)
TraLi7 = Translink.create!(supplysite_id: SuSi2.id, demandsite_id: DeSi3.id, unit_cost: 5, transport_quantity: 0.0)
TraLi8 = Translink.create!(supplysite_id: SuSi2.id, demandsite_id: DeSi4.id, unit_cost: 3, transport_quantity: 0.0)
TraLi9 = Translink.create!(supplysite_id: SuSi3.id, demandsite_id: DeSi1.id, unit_cost: 8, transport_quantity: 0.0)
TraLi10 = Translink.create!(supplysite_id: SuSi3.id, demandsite_id: DeSi2.id, unit_cost: 8, transport_quantity: 0.0)
TraLi11 = Translink.create!(supplysite_id: SuSi3.id, demandsite_id: DeSi3.id, unit_cost: 1, transport_quantity: 0.0)
TraLi12 = Translink.create!(supplysite_id: SuSi3.id, demandsite_id: DeSi4.id, unit_cost: 6, transport_quantity: 0.0)

Wir führen die Migration und Befüllung durch:

$ rails db:migrate:reset
$ rails db:seeds

Nun müssen wir in der Definition der Modelle wieder die Assoziationen geraderücken und diese in den Views auch verwenden!

Jeder Translink gehört sowohl zu einer Supplysite als auch zu einer Demandsite, also Einträge im 'model/translinks.rb':

  belongs_to :supplysite
  belongs_to :demandsite

Wir brauchen Verweise in 'model/supplysite.rb':


  has_many :translinks, :dependent => :destroy
  accepts_nested_attributes_for :translinks


  def supplysite_codename
    self.site.codename
  end

Analog in 'model/demandsite.rb':

  has_many :translinks, :dependent => :destroy
  accepts_nested_attributes_for :translinks


  def demandsite_codename
    self.site.codename
  end



Anpassung im Index-File:

  <td><%= translink.supplysite.site.codename %></td>
  <td><%= translink.demandsite.site.codename %></td>

Den Link auf 'show' löschen wir!

Wir passen die Datei '_form.html.erb' an:


  <div class="field">
    <%= form.label 'Angebotsort' %>
    <%= form.collection_select :supplysite_id, Supplysite.all, :id, :supplysite_codename %>
  </div>

  <div class="field">
    <%= form.label :demandsite_id %>
    <%= form.collection_select :demandsite_id, Demandsite.all, :id, :demandsite_codename %>
  </div>

Vom Index wollen wir auch zurückspringen können:

  <br>
  <%= link_to 'Zurück zur Transportplanung', transport_start_path %>



**********************  Teil 2: DataTables, BestInPlace und Optimierung

Zunächst wollen wir mit DataTables und BestInPlace etwas mehr Komfort!

Erläuterungen findet man im Railscast #340 und #302!


Im Gemfile ist schon alles vorbereitet!


In der Datei "application.js" fügen wir ein:

  //= require jquery.turbolinks
  //= require best_in_place
  //= require jquery_ujs
  //= require dataTables/jquery.dataTables



In der Datei "application.css" fügen wir ein:

  *= require jquery-ui/core
  *= require dataTables/src/demo_table_jui


Die Darstellung ab hier bezieht sich auf die Sites. Analog läuft es bei den Supplysites, den Demandsites und den Translinks!



In der Datei "sites.coffee" fügen wir ein:

  jQuery ->
    $('#sites').dataTable
      sPaginationType: "full_numbers"
      bJQueryUI: true

  jQuery ->
    $('.best_in_place').best_in_place()

Nun gehen wir an das die Datei "index.html.erb" für die Sites:

  <table  id="sites" class="display">
    <thead>
      <tr>
        <th>Name</th>
        <th>Codename</th>
        <th></th>
        <th></th>
        <th></th>
      </tr>
    </thead>


Damit wird der Index schon hübsch, aber einige Rücksprünge müssen Turbolinks ausschalten, dazu ergänzen wir in den Dateien "new.html.erb", "edit.html.erb", "show.html.erb" :

<%= link_to 'Back', sites_path, "data-turbolinks": "false" %>


Nun würden wir auch gerne via "BestInPlace" im Show-View die Anzeige des Ortsnamens direkt überschreiben können:

Vorher:

  <p>
    <strong>Name:</strong>
    <%= @site.name %>
  </p>


Nachher:

  <p>
    <strong>Name:</strong>
    <%= best_in_place @site, :name %>
  </p>


Hier noch ein cooler Hinweis: Mit der Zeile

  <td><%= best_in_place site.itself, :name %></td>

kann man auch direkt im Index-View den Namen in der Anzeige ändern... :-).


Nun machen wir uns an die Funktionalität für die Planung. Der erste Schritt ist eine Action im Translinks-Controller zum Löschen der Transportmengen!

  def delete_transportation_quantities

    if File.exist?("Transportmengen_v2.txt")
      File.delete("Transportmengen_v2.txt")
    end
    if File.exist?("Zielfunktionswert_v2.txt")
      File.delete("Zielfunktionswert_v2.txt")
    end

    @translinks = Translink.all
    @translinks.each { |li|
  #      li.transport_quantity=0.0
      li.transport_quantity=nil
      li.save
    }

    @objective_function_value=nil

    render :template => "translinks/index"

  end

Wir brauchen auch eine Route in "routes.rb", um dahin zu kommen:

  post 'translinks/delete_transportation_quantities', :to => 'translinks#delete_transportation_quantities'



Letztlich benötigen wir in der Startseite der Transportplanung "transport_start.html.erb" einen Aufrufbutton:


  <p>

  Nach der Eingabe der Daten können wir nun endlich optimieren!
  <br>

    <%= button_to "Lösche Transportmengen", :controller => 'translinks', :action =>  'delete_transportation_quantities' %>

  </p>

Jetzt könnten wir Transportmengen löschen, wenn wir welche hätten!

Analog bauen wir die anderen Actions ein:

In der Datei "routes.rb":

  post 'translinks/read_and_show_ofv', :to => 'translinks#read_and_show_ofv'
  post 'translinks/read_transportation_quantities', :to => 'translinks#read_transportation_quantities'
  post 'translinks/optimize', :to => 'translinks#optimize'



Im Translinks-Controller:


    def optimize

      if File.exist?("Transportmodell_v3_Input_Instanz1.inc")
        File.delete("Transportmodell_v3_Input_Instanz1.inc")
      end
      f=File.new("Transportmodell_v3_Input_Instanz1.inc", "w")
      printf(f, "set i / \n")
      @supplysites = Supplysite.all
      @supplysites.each { |ssi| printf(f, "i" + ssi.id.to_s + "\n") }
      printf(f, "/" + "\n\n")

      printf(f, "j / \n")
      @demandsites = Demandsite.all
      @demandsites.each { |dsi| printf(f, "j" + dsi.id.to_s + "\n") }
      printf(f, "/" + "\n\n")

      printf(f, "l / \n")
      @translinks = Translink.all
      @translinks.each { |li| printf(f, "l" + li.id.to_s + "\n") }
      printf(f, "/;" + "\n\n")


      printf(f, "LI(l,i) = no;\n")
      printf(f, "LJ(l,j) = no;\n\n")

      @translinks.each do |li|
        printf(f, "LI( 'l" + li.id.to_s + "', 'i" + li.supplysite_id.to_s + "') = yes;\n")
        printf(f, "LJ( 'l" + li.id.to_s + "', 'j" + li.demandsite_id.to_s + "') = yes;\n\n")
      end
      printf(f, "\n\n")

      printf(f, "Parameter\n  A(i) /\n")

      @supplysites.each { |so| printf(f, "i" + so.id.to_s + "  " + so.supply_quantity.to_s + "\n") }
      printf(f, "/" + "\n\n")

      printf(f, "\nN(j) /\n")

      @demandsites.each { |si| printf(f, "j" + si.id.to_s + "  " + si.demand_quantity.to_s + "\n") }
      printf(f, "/" + "\n\n")

      printf(f, "\nc(l) /\n")

      @translinks.each { |li| printf(f, "l" + li.id.to_s + "  " + li.unit_cost.to_s + "\n") }
      printf(f, "/" + "\n\n")

      printf(f, ";\n")
      f.close


      if File.exist?("Transportmengen_v2.txt")
        File.delete("Transportmengen_v2.txt")
      end

      system "C:\\GAMS\\win64\\24.5\\gams Transportmodell_v2"

  #    @translinks = Translink.all
      flash.now[:started] = "Die Rechnung wurde gestartet!"
      render 'static_pages/transport_start'

    end


    def read_transportation_quantities

      if File.exist?("Transportmengen_v2.txt")

        fi=File.open("Transportmengen_v2.txt", "r")
        fi.each { |line| # printf(f,line)
          sa=line.split(";")
          sa0=sa[0].delete "l "
          sa3=sa[3].delete " \n"
          al=Translink.find_by_id(sa0)
          al.transport_quantity=sa3
          al.save

        }
        fi.close
        @translinks = Translink.all
        render :template => "translinks/index"
      else
        flash.now[:not_available] = "Transportmengen wurden noch nicht berechnet!"
        @translinks = Translink.all
        render :template => "translinks/index"
      end
    end


    def read_and_show_ofv

      if File.exist?("Zielfunktionswert_v2.txt")
        fi=File.open("Zielfunktionswert_v2.txt", "r")
        line=fi.readline
        fi.close
        sa=line.split(" ")
        @objective_function_value=sa[1]
      else
        @objective_function_value=nil
        flash.now[:not_available] = "Zielfunktionswert wurde noch nicht berechnet!"

      end

      @translinks = Translink.all
      render :template => "translinks/index"
    end


In der Startseite für die Transportplanung fügen wir die fehlenden Buttons ein:

  <%= button_to "Optimiere", :controller => 'translinks', :action =>  'optimize'  %>
   <%= button_to "Lies die Transportmengen ein", :controller => 'translinks', :action =>  'read_transportation_quantities'  %>
   <%= button_to "Lies den Zielfunktionswert ein", :controller => 'translinks', :action =>  'read_and_show_ofv'  %>


Im Index-View für die Translinks lassen wir uns noch die Kosten anzeigen:


  <br>



  <% if @objective_function_value!=nil %>
      Die Kosten betragen <%=   @objective_function_value %> Geldeinheiten!   <br>   <br>
  <% end %>


Nun wäre es eine gute Idee, noch Tests einzubauen und sicherzustellen, dass kein Unfug eingegeben werden kann...
